var server;
function init(result) {
	db.open({
		server : 'here',
		version : 1,
		schema : {
			people : {
				key : {
					keyPath : 'id',
					autoIncrement : true
				},
				indexes : {
					firstName : {},
					age : {}
				}
			}
		}
	}).then(function (s) {
		server = s;
		$("#"+result).text("Database opened! " + s);
	});
}