<html>
    <head>
        <meta name="generator" content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <title>IndexedDB using db.js</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css" />
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
        <style>
li.L0, li.L1, li.L2, li.L3,
li.L5, li.L6, li.L7, li.L8
{ list-style-type: decimal !important }
</style>
    </head><?php
          function getScr($file){
                  return '<pre class="prettyprint lang-js linenums">'.htmlentities(file_get_contents("scripts/".$file.".js")).'</pre>';
          }
          ?>
    <body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">IndexedDB @ db.js</a>
            </div>
        </div>
    </nav>
    <div class="jumbotron">
        <div class="container">
            <div class="page-header">
                <h1>Show&amp;Tell: 
                <small>IndexedDB @ db.js</small></h1>
            </div>
            <p>I'll show you how we can use easily an IndexedDB functionality wrapped by db.js library.</p>
            <p>
                <small>All examples derived from 
                <a href="https://github.com/aaronpowell/db.js">https://github.com/aaronpowell/db.js</a></small>
            </p>
        </div>
    </div>
	
    <div class="container">
        <h2>Initialization</h2>
        <div class="container-fluid">
            <p>Each database (in db.js it's called server) must be inintialized before use. It's resemble the way, how connection to server side databases
            (like MySQL, Oracle) is established.</p>
            <div class="panel panel-primary">
                <div class="panel-heading">Initialize db</div>
                <div class="panel-body">
                    <?=getScr('init') ?>
                    <button class="btn btn-primary" type="button" onclick="init('initResult');">Run!</button><br>
					<div id="initResult"></div>
                </div>
            </div>
        </div>
		
        <h2>Populate with data</h2>
        <div class="container-fluid">
            <p>Common use of database is filling by data. Using indexedDB, we can add any kind of data (without declaring specification of object before use).
            It can contains data which we do not include in index definition of 'schema'.</p>
            <div class="panel panel-primary">
                <div class="panel-heading">Populate</div>
                <div class="panel-body">
                    <?=getScr('add') ?>
                    <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">First Name</span> 
                    <input type="text" id="firstName" class="form-control" /></div><br>
                    <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Last Name</span> 
                    <input type="text" id="lastName" class="form-control" /></div><br>
                    <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Age</span> 
                    <input type="text" id="age" class="form-control" /></div><br>
                    <button class="btn btn-primary" type="button"
                    onclick="addPerson($('#firstName').val(),$('#lastName').val(),$('#age').val());">Add!</button>
                </div>
            </div>
        </div>
		<h2>Query</h2>
		
		 <div class="container-fluid">
            <p>Database could by queried without any conditions.</p>
            <div class="panel panel-primary">
                <div class="panel-heading">Query for all</div>
                <div class="panel-body">
                    <?=getScr('queryAll') ?>
                    <button class="btn btn-primary" type="button" onclick="queryAll('#queryAllResult');">Run!</button><br>
					<div id="queryAllResult"></div>
                </div>
            </div>
        </div>
		
        <div class="container-fluid">
            <p>Database is able to query using  </p>
            <div class="panel panel-primary">
                <div class="panel-heading">Query by id</div>
                <div class="panel-body">
                    <?=getScr('queryById') ?>
					<div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Id</span> 
                    <input type="text" id="queryId" class="form-control" /></div><br>
                    <button class="btn btn-primary" type="button" onclick="queryById($('#queryId').val(),'#queryIdResult');">Run!</button><br>
					<div id="queryIdResult"></div>
                </div>
            </div>
        </div>
		
		<div class="container-fluid">
            <p>Database is able to query using index.</p>
            <div class="panel panel-primary">
                <div class="panel-heading">Query by name</div>
                <div class="panel-body">
                    <?=getScr('queryByName') ?>
					<div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">First Name</span> 
                    <input type="text" id="queryFirstName" class="form-control" /></div> <br>
                    <button class="btn btn-primary" type="button" onclick="queryByName($('#lastName').val(),'#queryNameResult');">Run!</button><br>
					<div id="queryNameResult"></div>
                </div>
            </div>
        </div>
		
        <h2>Remove the data!</h2>
        <div class="container-fluid">
            <p>Databases could clear all data.</p>
            <div class="panel panel-primary">
                <div class="panel-heading">Clear all</div>
                <div class="panel-body">
                    <?=getScr('clear') ?>
                    <button class="btn btn-primary" type="button" onclick="clearPeopleDatabase();">Run!</button>
                </div>
            </div>
        </div>
    </div>
    <script src="db.js"></script> 
    <script src="script.js"></script></body>
</html>
