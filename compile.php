<?php


$scriptsPath = "scripts/";
$scripts = ['init.js','add.js','queryById.js','clear.js','queryByName.js','queryAll.js'];

$outFile = "script.js";
$out  = '';
foreach($scripts as $script){
	$out .= file_get_contents($scriptsPath.$script)."\r\n";
}

file_put_contents($outFile,$out);
